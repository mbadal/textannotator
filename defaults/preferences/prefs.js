pref("extensions.textannotator.boolpref", false);
pref("extensions.textannotator.intpref", 138);
pref("extensions.textannotator.stringpref", "A string");

// https://developer.mozilla.org/en/Localizing_extension_descriptions
pref("extensions.text@matejbadal.com.description", "chrome://textannotator/locale/overlay.properties");
