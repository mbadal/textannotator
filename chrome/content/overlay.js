function alertConsole() {
    Application.console.log('Click');
    alert("test");
}

function addClickEvent(instance) {
    var tabs = document.getElementsByClassName('tabmail-tab');
    /*
     * occurs when selecting tabs
     **/
    var mailTree = document.getElementById('threadTree');
    var path = getAppPath("texthighlighter");
    mailTree.addEventListener('click', function() {
        var messageBody = document.getElementById('messagepane').contentDocument.body;
        var newBody = instance.annotate(messageBody.innerHTML, path);
        messageBody.innerHTML = newBody;
        var newTabs = document.getElementsByClassName('tabmail-tab');
    }, true);
    
    /**
     * add event to default tab
     **/
    tabs[0].addEventListener('click', function(){
        var messageBody = document.getElementById('messagepane').contentDocument.body;
        var newBody = instance.annotate(messageBody.innerHTML, path);
        messageBody.innerHTML = newBody;
    }, true);
}

function sendPostRequest(host, data) {
    var request;
    var i = 0;
    var loader = document.getElementById('image1');
    var statusLabel = document.getElementById('status_label');
    var label = 'Text Highlighter: ';

    if (window.XMLHttpRequest) {
        //IE7+, Firefox, Opera, Chrome, Safari
        request = new XMLHttpRequest();
    } else {
        //Explorer
        request = new ActiveObject('Microsoft.XMLHTTP');
    }

    request.onreadystatechange = function() {
        //alert("State of request: " + request.readyState);
        if (request.readyState == 4 && request.status == 200) {
            //alert(request.responseText);
            loader.style.display = 'none';
            statusLabel.label = label + 'Anotation done';
            var messagePane = document.getElementById('messagepane');
            messagePane.contentDocument.body.innerHTML = request.responseText;
        } else if (request.readyState == 1 && i == 0) {
            //onSend

            i++;
            //alert('Request sent');
            statusLabel.label = label + 'Processing...'
            loader.style.display = 'block';
        } /*else if (request.readyState == 4 && request.status != 200) {
            alert('Application encourted error while proccessing request');
        }*/
    }

    request.open('POST', host, true);
    request.setRequestHeader('Content-type', 'text/plain');
    request.send(data);
}

function sendGetRequest(host) {
    var request;
    var i = 0;
    var loader = document.getElementById('status_label');
    if (window.XMLHttpRequest) {
        //IE7+, Firefox, Opera, Chrome, Safari
        request = new XMLHttpRequest();
        //alert('Moz');
    } else {
        //Explorer
        request = new ActiveObject('Microsoft.XMLHTTP');
        //alert('Microsoft'); 
    }

    request.onreadystatechange = function() {
        //alert("State of request: " + request.readyState);
        if (request.readyState == 4 && request.status == 200) {
            //onComplete
            alert('Request succesfully done');
            alert(request.responseText);
        } else if (request.readyState == 1 && i == 0) {
            //onSend

            //prevents from multiple occuring
            i++;
            alert('Request sent');
        }
    }
    request.open('GET', host, true);
    //request.setRequestHeader("Content-Type", "text/plain");
    request.send();
}

window.addEventListener('load', function(e) {
    var button = document.getElementById('test_button');
    button.addEventListener('click', function(e) {
        //alert('button pressed!');
        var messagePane = document.getElementById('messagepane');
        var data = messagePane.contentDocument.body.innerHTML;
        sendPostRequest('http://localhost:8080/annotation/automatic', data);
    });

    var mailTree = document.getElementById('threadTree');
    
    var messagePane = document.getElementById('messagepane');
    messagePane.addEventListener('load', function() {
        /*var messagePane = document.getElementById('messagepane');
        var data = messagePane.contentDocument.body.innerHTML;
        sendPostRequest('http://localhost:8080/annotation/automatic', data);*/
    }, true);
    Application.console.log('Succes');
});