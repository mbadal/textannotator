############### Inštalácia ##################
Súbory s prídavný modulom stačí rozbaliť do súboru s rozšíreniami pre Thunderbird.
Thunderbird sám by mal toto rozšírenie zaregistrovať a ponúknuť Vám jeho inštaláciu.

############### Spustenie ###################
Prídavný modul tvoria dve časti - samotný kód modulu a server, ktorý zachytáva REST-ovské requesty.
Tento server musíte spustiť manuálne, spustením RestServer.jar, ktorý sa nachádza v priečinku "java" v súborovej štruktúre modulu.
Na ľavej strane gui Thunderbirdu by Vám mali pribudnúť nové prvky - tlačidlo test.
Modul momentálne funguje tak, že si vyberiete správu, ktorú by ste chceli značkovať. Táto správa sa zobrazí v dolnej polovicu gui TB.
Po kliknutí na tlačidlo test, sa odošle request na server, ktorý sa následne vyhodnotí. Následne sa zobrazí výsledok značkovania.
je možné, že sú tam buggy, ja osobne som skúšal značkovanie lokácií a organizácii. 
Server počúva requesty na porte 8080. 
